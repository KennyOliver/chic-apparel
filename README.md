# chic-apparel
<img src="https://user-images.githubusercontent.com/70860732/113517405-9fd33d00-9577-11eb-9fcf-59aa19d630e7.jpeg" width="50%" align="right">

![CodeFactor](https://www.codefactor.io/repository/github/KennyOliver/chic-apparel/badge?style=for-the-badge)
![Latest SemVer](https://img.shields.io/github/v/tag/KennyOliver/chic-apparel?label=version&sort=semver&style=for-the-badge)
![Repo Size](https://img.shields.io/github/repo-size/KennyOliver/chic-apparel?style=for-the-badge)
![Total Lines](https://img.shields.io/tokei/lines/github/KennyOliver/chic-apparel?style=for-the-badge)

[![repl](https://replit.com/badge/github/KennyOliver/chic-apparel)](https://replit.com/@KennyOliver/chic-apparel)

**Handmade static mock e-commerce site**

[![GitHub Pages](https://img.shields.io/badge/See%20Demo-252525?style=for-the-badge&logo=safari&logoColor=white&link=https://kennyoliver.github.io/chic-apparel)](https://kennyoliver.github.io/chic-apparel)

---
Kenny Oliver ©2021
